package com.example.owner.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SubActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        //入力された画面から値の受け取り
        Intent subIntent = getIntent();
        Bundle extras = subIntent.getExtras();
        String strName = "";
        String getHeight = "";
        String getWeight = "";
        if(extras != null){
            strName = extras.getString("in_name");
            getHeight = extras.getString("in_height");
            getWeight = extras.getString("in_weight");
        }

        TextView subResult = (TextView) findViewById(R.id.subResult);
//        subResult.setText(getWeight);

        String strMsg = "";
        String hantei = "";
        String hantei_num = "";
        if (strName.equals("") || getHeight.equals("") || getWeight.equals("")) {
            //未入力の場合
            strMsg = "全部入力してください！！";
        } else {
            //floatに変更
            float fheight = Float.parseFloat(getHeight);
            float fweight = Float.parseFloat(getWeight);
            //入力あり
            fheight = fheight/100;
            Float kekka = fweight / (fheight * fheight);
            if (kekka < 18.5) {
                hantei = "低体重";
                hantei_num = "18.5";
                hantei_num = "18.5より下";
            } else if (kekka < 25) {
                hantei = "標準体重";
                hantei_num = "18.5から25まで";
            } else if (kekka < 35) {
                hantei = "過体重";
                hantei_num = "25から35まで";
            } else {
                hantei = "肥満";
                hantei_num = "35以上";
            }
            strMsg = strName + "さんのBMI結果は";
            String strkekka = String.format("%.2f", kekka);
            strMsg += hantei + "です。";
            strMsg += "BMI数値は"+strkekka+"でした。";
            strMsg += "("+hantei+"のラインは"+hantei_num+"です)";
        }
        subResult.setText(strMsg);
    }
    //戻るボタンを押した時
    public void btnBackClick(View myView){
        finish();
    }
}
