package com.example.owner.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener
{

    @Override
    public void onClick(View v) {
        // 押下時の処理
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView txtResult = (TextView)  findViewById(R.id.txtvResult);
        txtResult.setText("");

        EditText edtext1 = (EditText)findViewById(R.id.edName);
        edtext1.setText("");
        EditText edtext2 = (EditText)findViewById(R.id.edHeight);
        edtext2.setText("");
        EditText edtext3 = (EditText)findViewById(R.id.edWeight);
        edtext3.setText("");

        //コールバックメソッドの実装
        Button btn1 = (Button)findViewById(R.id.btnStart);
        btn1.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        dispstart();
                    }
                }
        );

        //コールバックメソッドの実装
        Button btn2 = (Button)findViewById(R.id.btnClear);
        btn2.setOnClickListener(
            new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    dispclear();
                }
            }
        );
    }

    public void dispclear(){
        //EditTextの初期化
        EditText edtext1 = (EditText)findViewById(R.id.edName);
        edtext1.setText("");
        EditText edtext2 = (EditText)findViewById(R.id.edHeight);
        edtext2.setText("");
        EditText edtext3 = (EditText)findViewById(R.id.edWeight);
        edtext3.setText("");
        TextView txtresult = (TextView)findViewById(R.id.txtvResult);
        txtresult.setText("");
    }


//    public void ButtonClick(View MyView) {
    public void dispstart() {

        TextView txtResult = (TextView) findViewById(R.id.txtvResult);

        //EditTextから文字列を取得
        EditText edName = (EditText) findViewById(R.id.edName);
        String strName = edName.getText().toString();

        EditText edHeight = (EditText)findViewById(R.id.edHeight);
        String getHeight = edHeight.getText().toString();


        EditText edWeight = (EditText) findViewById(R.id.edWeight);
        String getWeight = edWeight.getText().toString();

        String strMsg = "";
        String hantei = "";
        String hantei_num = "";
        if (strName.equals("") || getHeight.equals("") || getWeight.equals("")) {
            //未入力の場合
            strMsg = "全部入力してください！！";
        } else {
            //floatに変更
            float fheight = Float.parseFloat(getHeight);
            float fweight = Float.parseFloat(getWeight);
            //入力あり
            fheight = fheight/100;
            Float kekka = fweight / (fheight * fheight);
            if (kekka < 18.5) {
                hantei = "低体重";
                hantei_num = "18.5";
                hantei_num = "18.5より下";
            } else if (kekka < 25) {
                hantei = "標準体重";
                hantei_num = "18.5から25まで";
            } else if (kekka < 35) {
                hantei = "過体重";
                hantei_num = "25から35まで";
            } else {
                hantei = "肥満";
                hantei_num = "35以上";
            }
            strMsg = strName + "さんのBMI結果は";
            String strkekka = String.format("%.2f", kekka);
            strMsg += hantei + "です。";
            strMsg += "BMI数値は"+strkekka+"でした。";
            strMsg += "("+hantei+"のラインは"+hantei_num+"です)";
        }
        txtResult.setText(strMsg);
    }

    //次の画面へボタンを押した時の処理
    public void btnNextClick(View myView){
        //Intentクラスを元にしたオブジェクトの作成
        //Intent:画面間の橋
        //引数:自分自身のクラス(画面)、起動したい画面のクラス
        Intent subIntent = new Intent(MainActivity.this,SubActivity.class);
        //Intentに対し、値を置いていく

        //EditTextから文字列を取得
        EditText edName = (EditText) findViewById(R.id.edName);
        String strName = edName.getText().toString();
        EditText edHeight = (EditText)findViewById(R.id.edHeight);
        String getHeight = edHeight.getText().toString();
        EditText edWeight = (EditText) findViewById(R.id.edWeight);
        String getWeight = edWeight.getText().toString();

        subIntent.putExtra("in_name",strName);
        subIntent.putExtra("in_height",getHeight);
        subIntent.putExtra("in_weight",getWeight);

        //画面の起動
        startActivity(subIntent);
    }

}
